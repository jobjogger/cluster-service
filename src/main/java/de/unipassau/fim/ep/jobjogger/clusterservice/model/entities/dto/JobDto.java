package de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * A job is a calculation started from a dockerimage.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobDto {
    @NotNull
    private Long id;
    @NotNull
    private String imageId;
    @NotNull
    private String name;
    private JobStatus status;
    private String logs;
    private List<JobEnvDto> environmentVariables;
    private JobMetaDto meta;
    @NotNull
    private String userId;
    @NotNull
    private String priority;
}
