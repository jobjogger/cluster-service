package de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Job environment variables
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobEnvDto {
    private String name;
    private String value;
}
