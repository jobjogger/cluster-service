package de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Job meta data
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobMetaDto {
    private String createTime;
    private String pendingTime;
    private String startTime;
    private String endTime;
}
