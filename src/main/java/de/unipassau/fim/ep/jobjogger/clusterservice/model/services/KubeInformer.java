package de.unipassau.fim.ep.jobjogger.clusterservice.model.services;

import static java.lang.Long.parseLong;

import de.unipassau.fim.ep.jobjogger.clusterservice.application.CancelingStore;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto.JobDto;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto.JobStatus;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.informer.ResourceEventHandler;
import io.kubernetes.client.informer.SharedIndexInformer;
import io.kubernetes.client.informer.SharedInformerFactory;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.util.CallGeneratorParams;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * The event handler triggered from users' pods.
 */
@Service
@Slf4j
public class KubeInformer {

    @Value("${microservice-secret.header-name}")
    private String principalRequestHeader;

    @Value("${microservice-secret.secret-token}")
    private String principalRequestValue;

    private final
    CoreV1Api coreV1Api;

    private final
    LogService logService;

    private final
    PodService podService;

    private final
    SharedInformerFactory factory;

    private final
    RestTemplate restTemplate;

    private final
    CancelingStore cancelingStore;

    @Value("${host.resource-server}")
    private String resourceServerHost;

    /**
     * KubeInformer constructor
     *
     * @param coreV1Api Api version
     * @param logService log service
     * @param sharedInformerFactory Informer Factory from Kubernetes
     * @param restTemplate http template
     * @param podService pod service
     * @param cancelingStore store for canceling jobs
     */
    public KubeInformer(CoreV1Api coreV1Api, LogService logService, SharedInformerFactory sharedInformerFactory,
                        RestTemplate restTemplate, PodService podService, CancelingStore cancelingStore) {
        this.coreV1Api = coreV1Api;
        this.logService = logService;
        this.factory = sharedInformerFactory;
        this.restTemplate = restTemplate;
        this.podService = podService;
        this.cancelingStore = cancelingStore;
    }


    /**
     * The Informer handles all events from the pods which belong to users.
     */
    @PostConstruct
    public void informer() {

        // Pod Informer
        SharedIndexInformer<V1Pod> podInformer =
                factory.sharedIndexInformerFor(
                        (CallGeneratorParams params) -> {
                            return coreV1Api.listPodForAllNamespacesCall(
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    params.resourceVersion,
                                    params.timeoutSeconds,
                                    params.watch,
                                    null,
                                    null);
                        },
                        V1Pod.class,
                        V1PodList.class);

        podInformer.addEventHandler(
                new ResourceEventHandler<V1Pod>() {
                    @Override
                    public void onAdd(V1Pod obj) {
                        log.info("Added Pod: " + obj.getMetadata().getName());
                    }

                    @Override
                    public void onUpdate(V1Pod oldObj, V1Pod newObj) {
                        if (!oldObj.getMetadata().getNamespace().equals("default")) {
                            String podPhase = newObj.getStatus().getPhase();
                            boolean isCanceling = cancelingStore.isCanceling(newObj.getMetadata().getNamespace(),
                                    newObj.getMetadata().getName());

                            //Only updates if phase has changed.
                            if (!oldObj.getStatus().getPhase().equals(podPhase)  && !isCanceling) {

                                // Fetch logs if pod succeeded
                                if (podPhase.equals("Succeeded") || podPhase.equals("Failed")) {
                                    postLogsAndDelete(newObj.getMetadata().getNamespace(),
                                            newObj.getMetadata().getName());
                                } else {
                                    //Post pod status update
                                    postUpdateStatus(newObj.getMetadata().getNamespace(), newObj.getMetadata().getName(), podPhase);
                                }
                            }
                        }
                    }

                    @Override
                    public void onDelete(V1Pod obj, boolean deletedFinalStateUnknown) {
                        if (!obj.getMetadata().getNamespace().equals("default")) {
                            boolean isCanceling = cancelingStore.isCanceling(obj.getMetadata().getNamespace(),
                                    obj.getMetadata().getName());

                            // If the job was cancelled manually the custom status cancelled is posted.
                            if (isCanceling) {
                                postUpdateStatus(obj.getMetadata().getNamespace(), obj.getMetadata().getName(),
                                        "Canceled");
                            } else {
                                postUpdateStatus(obj.getMetadata().getNamespace(), obj.getMetadata().getName(),
                                        obj.getStatus().getPhase());
                            }
                            cancelingStore.removeCanceling(obj.getMetadata().getNamespace(),
                                    obj.getMetadata().getName());
                            log.info("Deleted pod: " + obj.getMetadata().getName());
                        }
                    }
                });


        factory.startAllRegisteredInformers();
    }

    private void postUpdateStatus(String username, String id, String status) {
        String resourceStatusURL = resourceServerHost + "/cluster/users/" + username + "/jobs/" + id + "/status";
        JobDto updateJob = new JobDto();
        updateJob.setStatus(JobStatus.valueOf(status.toUpperCase()));
        HttpEntity<JobDto> request = new HttpEntity<>(updateJob, getHeaders());
        try {
           restTemplate.postForEntity(resourceStatusURL, request, String.class);
        }catch (HttpStatusCodeException exc){
            log.error("Could not post to resource server. Status code: " + exc.getStatusCode());
        }
        log.info("Posted status (" + status + ") to url: " + resourceStatusURL);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(principalRequestHeader, "Bearer " + principalRequestValue);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * Post Job Logs to resource server and the deletes the pod.
     *
     * @param username is the username of the pod owner.
     * @param id       is the id of the job/pod.
     * @return Status code Ok if everything worked and Internal Sever Error is something went wrong.
     */
    public ResponseEntity<String> postLogsAndDelete(String username, String id) {
        String logs = "";

        try {
            logs = logService.getLogsString(id, username);
        } catch (ApiException e) {
            log.error(e.toString());
            log.info("Something went wrong with the Api when trying to retrieve the logs. ");
            log.info(e.getResponseBody());
            return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Posting log to resource-server
        log.info("Posting log from Pod " + username + "-" + id + " to resource server");
        String resourceLogURL = resourceServerHost + "/cluster/users/" + username + "/jobs/" + id + "/logs";
        JobDto updateJob = new JobDto();
        updateJob.setLogs(logs);
        updateJob.setId(parseLong(id));
        HttpEntity<JobDto> request = new HttpEntity<>(updateJob, getHeaders());
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(resourceLogURL, request, String.class);
        }catch(HttpStatusCodeException exc){
            log.error("Posting logs failed. Status-Code: " + exc.getStatusCode() + "\n"+
                    "Body: " + exc.getResponseBodyAsString());
            return new ResponseEntity<>("Posting job status to resource server failed. Response: " +
                    exc.getResponseBodyAsString(),exc.getStatusCode());
        }

        // If everything worked the delete Pod
        try {
            podService.deletePodByName(username, id);
        } catch (ApiException e) {
            log.error("Could not delete pod: " + username + "-" + id);
            log.error(e.toString());
            return new ResponseEntity<>("Kubernetes API exception occurred: " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String msg = "All data from Pod " + username + "-" + id + "is persisted and Job is deleted";
        log.info(msg);
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }
}
