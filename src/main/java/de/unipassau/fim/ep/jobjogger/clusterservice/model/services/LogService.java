package de.unipassau.fim.ep.jobjogger.clusterservice.model.services;

import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
import org.springframework.stereotype.Service;

/**
 * Service class managing reading logs of pods inside the kubernetes cluster.
 */
@Service
public class LogService {

    private final
    CoreV1Api api;

    /**
     * Log service constructor
     *
     * @param api api version
     */
    public LogService(CoreV1Api api) {
        this.api = api;
    }


    /**
     * Return the logs of a pod.
     *
     * @throws ApiException if there is a problem inside kubernetes.
     */
    public String getLogsString(String name, String namespace) throws ApiException {
        return api.readNamespacedPodLog(name, namespace, null, null, null, null,
                null, null, null, null);
    }
}
