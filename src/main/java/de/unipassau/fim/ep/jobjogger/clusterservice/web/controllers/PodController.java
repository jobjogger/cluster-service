package de.unipassau.fim.ep.jobjogger.clusterservice.web.controllers;

import de.unipassau.fim.ep.jobjogger.clusterservice.application.CancelingStore;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto.JobDto;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.services.KubeInformer;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.services.PodService;
import io.kubernetes.client.ApiException;
import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This Controller handles all the pod related requests from the resource server.
 */
@RestController
@Slf4j
public class PodController {

    private final PodService podService;

    private final KubeInformer kubeInformer;

    private final CancelingStore cancelingStore;

    public PodController(PodService podService, KubeInformer kubeInformer, CancelingStore cancelingStore) {
        this.podService = podService;
        this.kubeInformer = kubeInformer;
        this.cancelingStore = cancelingStore;
    }

    /**
     * Controller for adding a pod by a specific user.
     */
    @PostMapping("/{userId}/{jobId}")
    public  ResponseEntity<String> addPod(@NotNull @PathVariable String userId,
                                          @NotNull @PathVariable Long jobId,
                                          @RequestBody JobDto job) {
        try {
            log.info("Received request to start Pod with ID="+job.getId()+ " with priority=" + job.getPriority() + " with image="+job.getImageId()+" in namespace "+job.getUserId());
            podService.buildPod(jobId, userId, job.getImageId(), job.getEnvironmentVariables(), job.getPriority());
            return new ResponseEntity<>("Pod created.",HttpStatus.OK);
        } catch (ApiException | InterruptedException e) {
            log.error(e.toString());
            return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete pod by name.
     *
     * @param userId namespace ofthe pod.
     * @param jobId name of the pod.
     * @return HttpResponse
     */
    @DeleteMapping("/{userId}/{jobId}")
    public ResponseEntity<String> deletePod(@PathVariable String userId,
                                            @PathVariable Long jobId) {
        cancelingStore.addCancelling(userId, jobId.toString());
        return kubeInformer.postLogsAndDelete(userId, String.valueOf(jobId));
    }
}
