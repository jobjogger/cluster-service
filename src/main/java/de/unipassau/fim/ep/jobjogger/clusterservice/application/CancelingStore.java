package de.unipassau.fim.ep.jobjogger.clusterservice.application;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Store which saves if a job is currently cancelling or not.
 */
@Service
public class CancelingStore {

    public List<String> cancellingJobs = new ArrayList<>();

    /**
     * Add a job to the canceling state list.
     *
     * @param username of the person who holds the job
     * @param id of the job
     */
    public void addCancelling(String username, String id){
        cancellingJobs.add(username + "-" + id);
    }

    /**
     * Check if a job is currently canceling.
     *
     * @param username of the person who holds the job
     * @param id of the job
     * @return Bool if the Job is cancelling
     */
    public boolean isCanceling(String username, String id){
        return cancellingJobs.contains(username + "-" + id);
    }

    /**
     * Removes a job from the canceling state list.
     *
     * @param username of the person who holds the job
     * @param id of the job
     */
    public void removeCanceling(String username, String id){
        cancellingJobs.remove(username + "-" + id);
    }
}
