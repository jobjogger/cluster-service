package de.unipassau.fim.ep.jobjogger.clusterservice.application.config;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

/**
 * Class used for general bean definition.
 */
@Configuration
public class SpringConfig {

    /**
     * RestTemplate for performing HTTP requests to the resource-server.
     *
     * @return RestTemplate object.
     */
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        return restTemplate;
    }

    /**
     * Constructor to create ObjectMapper to create default JsonFactory
     *
     * @return ObjectMapper
     */
    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }

}
