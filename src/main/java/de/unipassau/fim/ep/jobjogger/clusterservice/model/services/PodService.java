package de.unipassau.fim.ep.jobjogger.clusterservice.model.services;

import de.unipassau.fim.ep.jobjogger.clusterservice.application.exceptions.AlreadyExistsException;
import de.unipassau.fim.ep.jobjogger.clusterservice.application.exceptions.EntityNotFoundException;
import de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto.JobEnvDto;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1EnvVar;
import io.kubernetes.client.models.V1EnvVarBuilder;
import io.kubernetes.client.models.V1Namespace;
import io.kubernetes.client.models.V1NamespaceBuilder;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodBuilder;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This service class manages pod creation and deletion using kubernetes API.
 */
@Service
@Slf4j
public class PodService {

    private final
    CoreV1Api api;

    @Value("${host.registry}")
    private String registryHost;

    @Value("${token-timeout}")
    private long timeout;

    /**
     * Pod service constructor
     *
     * @param api api version
     */
    public PodService(CoreV1Api api) {
        this.api = api;
    }

    /**
     * Creates a new Pod.
     *
     * @param jobId    name of the Pod.
     * @param imageId  id of the image.
     * @param userId   namespace the pod should run in.
     * @param envVars  envVars for the Pod
     * @param priority priority of the Pod
     * @throws AlreadyExistsException if the Pod already exists.
     */
    public void buildPod(Long jobId, String userId, String imageId, List<JobEnvDto> envVars, String priority)
            throws AlreadyExistsException, ApiException, InterruptedException {
        Map<String, String> podLabel = new HashMap<>();
        podLabel.put("name", jobId.toString());
        V1Pod pod =
                new V1PodBuilder()
                        .withNewMetadata()
                        .withName(jobId.toString())
                        .withLabels(podLabel)
                        .endMetadata()
                        .withNewSpec()
                        .withRestartPolicy("Never")
                        .withPriorityClassName(priority)
                        .addNewContainer()
                        .withName(jobId.toString())
                        .withImage(imageRegistryUrl(imageId))
                        .withEnv(convertToEnvVars(envVars))
                        .endContainer()
                        .endSpec()
                        .build();

        // Build Namespace if it doesnt exist.
        try {
            buildNameSpace(userId);
        } catch (AlreadyExistsException e) {
            log.info(e.getMessage());
        }

        createPod(jobId, userId, imageId, pod);
    }

    /**
     * Delete a pod from the cluster by the name.
     *
     * @param name the name of the pod.
     * @throws ApiException if api request is n ot successful.
     */
    public void deletePodByName(String namespace, String name) throws ApiException {
        log.info("Deleting pod " + name + " in namespace " + namespace + "...");
        try {
            api.deleteNamespacedPod(name, namespace, null, null, null, null, null, null);
        } catch (Exception e) {
            if (e.getCause() instanceof java.lang.IllegalStateException) {
                //ignoring Exception because of issue in kubernetes-client (#86)
            } else {
                if (e instanceof io.kubernetes.client.ApiException && e.getMessage().equals("Not Found")) {
                    throw new EntityNotFoundException("Job with name " + name + " in namespace " + namespace + " could not be found.");
                } else {
                    throw e;
                }
            }
        }

    }

    private void buildNameSpace(String name) throws AlreadyExistsException, ApiException {
        Map<String, String> nameSpaceLabel = new HashMap<>();
        nameSpaceLabel.put("name", name);
        V1Namespace namespace = new V1NamespaceBuilder()
                .withKind("Namespace")
                .withApiVersion("v1")
                .withNewMetadata()
                .withName(name)
                .withLabels(nameSpaceLabel)
                .endMetadata()
                .build();

        try {
            api.createNamespace(namespace, null, null, null);
            log.info("Created Namespace: " + name);
        } catch (ApiException e) {
            if (e.getMessage().equals("Conflict")) {
                throw new AlreadyExistsException("Namespace " + name + " already exists.");
            } else {
                throw e;
            }
        }
    }

    private String imageRegistryUrl(String imageId) {
        return registryHost + "/" + imageId;
    }

    private List<V1EnvVar> convertToEnvVars(List<JobEnvDto> rawEnvVars) {
        List<V1EnvVar> envVars = new LinkedList<>();
        if (rawEnvVars == null) {
            return envVars;
        }
        for (JobEnvDto rawEnvVar : rawEnvVars) {
            log.info("Env var found: Key = " + rawEnvVar.getName() + ". Value: " + rawEnvVar.getValue());
            V1EnvVar envVar = new V1EnvVarBuilder()
                    .withName(rawEnvVar.getName())
                    .withValue(rawEnvVar.getValue())
                    .build();
            envVars.add(envVar);
        }
        return envVars;
    }

    private void createPod(Long jobId, String userId, String imageId, V1Pod pod)
            throws ApiException, InterruptedException {
        log.info("Checking if namespace: " + userId + " has created token for default serivce account");

        // Check if token got created. After 10 sec (standard) throw error.
        // This is a workaround for a kubernetes bug that occurs when initialising a new namespace and creating a new pod.
        for (int count = 0; count < 10; count++) {
            if (api.listNamespacedSecret(userId, null, null, null, null, null, null, null, false).getItems().isEmpty()) {
                Thread.sleep(timeout);
            } else {
                break;
            }
            if (count == 9) {
                log.info("Default Service account token took too long to create");
                throw new ApiException("Default Service account token took too long to create");
            }
        }

        log.info("Creating pod; name: " + jobId + ", namespace: " + userId + " image: " + imageRegistryUrl(imageId));
        try {
            api.createNamespacedPod(userId, pod, null, null, null);
            log.info("Pod with name=" + jobId + " created in namespace " + userId);
        } catch (ApiException e) {
            log.info(e.getResponseBody());
            if (e.getMessage().equals("Conflict")) {
                throw new AlreadyExistsException("Pod already exists in namespace " + userId);
            } else {
                throw e;
            }
        }
    }
}
