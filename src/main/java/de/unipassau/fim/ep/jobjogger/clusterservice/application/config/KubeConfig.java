package de.unipassau.fim.ep.jobjogger.clusterservice.application.config;

import com.squareup.okhttp.OkHttpClient;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.informer.SharedInformerFactory;
import io.kubernetes.client.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * A simple example of how to use the Java API from an application outside a kubernetes cluster
 *
 * <p>Easiest way to run this: mvn exec:java
 * -Dexec.mainClass="io.kubernetes.client.examples.KubeConfigFileClientExample"
 *
 */
@Component
public class KubeConfig {

    /**
     * Configures default Kubernetes Client
     *
     * @return the Client when it is found
     * @throws IOException if error happened with Kubernetes
     */
    @Bean
    public CoreV1Api configureDefaultKubernetesClient() throws IOException{
        ApiClient client = Config.defaultClient();

        OkHttpClient httpClient =
                client.getHttpClient();

        httpClient.setReadTimeout(0, TimeUnit.SECONDS);

        client.setHttpClient(httpClient);

        Configuration.setDefaultApiClient(client);

        return new CoreV1Api(client);
    }

    /**
     * Gets the Node informer from Kubernetes
     *
     * @return Shared informer
     */
    @Bean
    public SharedInformerFactory configureInformer(){
        return new SharedInformerFactory();
    }
}