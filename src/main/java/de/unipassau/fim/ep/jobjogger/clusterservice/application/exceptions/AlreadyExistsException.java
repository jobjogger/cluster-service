package de.unipassau.fim.ep.jobjogger.clusterservice.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If entity in kubernetes already exist, throw AlreadyExistsException.
 */
@ResponseStatus(HttpStatus.CONFLICT)
@Slf4j
public class AlreadyExistsException extends RuntimeException{

    /**
     * Constructor for the Exception
     *
     * @param msg Error message
     */
    public AlreadyExistsException(String msg){
        super(msg);
        log.error("AlreadyExistsException: "+ msg);
    }
}
