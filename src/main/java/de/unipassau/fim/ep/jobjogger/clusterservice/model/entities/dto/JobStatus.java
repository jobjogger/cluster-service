package de.unipassau.fim.ep.jobjogger.clusterservice.model.entities.dto;

/**
 * All supported Job statuses
 */
public enum JobStatus {
    RUNNING, PENDING, STARTING, CANCELLING, FAILED, SUCCEEDED, CANCELED, NONE
}
