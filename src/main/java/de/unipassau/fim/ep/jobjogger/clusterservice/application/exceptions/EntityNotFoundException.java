package de.unipassau.fim.ep.jobjogger.clusterservice.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If entity in Kubernetes was not found, throw EntityNotFoundException.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException{

    /**
     * Constructor for the Exception
     *
     * @param msg Error message
     */
    public EntityNotFoundException(String msg){
        super(msg);
    }
}
