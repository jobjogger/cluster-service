# JobJogger Cluster Service
This repository contains the code of our JobJogger Cluster Service. 
It monitors and manages the cluster, i.e. start and stop pods (Jobs) 
as well as communicate status updates to the Resource Server.

## Environment Variables
| Name                     | Description                                             |
|--------------------------|---------------------------------------------------------|
| RESOURCE_SERVER_HOST     | Hostname of the Resource Server                         |
| REGISTRY_HOST            | Hostname of the Docker Registry which holds user Images |
| MICROSERVICE_COMM_SECRET | Secret for internal communication between the services  |
